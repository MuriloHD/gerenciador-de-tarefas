# gerenciador de tarefas

* [Informações gerais](#informações-geral)
* [Tecnologias](#tecnologias)
* [Rodando o projeto](#rodando-o-projeto)
* [Git Workflow](#git-workflow)
* [Sobre mensagens de commits](#sobre-mensagens-de-commits)
* [Ambientes da aplicação](#ambientes-da-aplicação)

## Informações gerais
Desenvolvimento de APP para a gestão de tarefas da uma empresa.
	
##  Tecnologias
Este projeto é criado com:
* npm 8.3.1
* node 16.14.0
* vite.js
* React.js 
* Ycodfy (Para automatizar o backend e a criacao do banco de dados)
	
## Rodando o projeto
* Faça git clone do projeto no repositório 

* Aplique o comando npm run dev no diretorio do projeto 

* Certifique-se de ter uma rede para o acesso dos endpoints do projeto na plataforma da Ycodfy


## Git Workflow

O Git Workflow é uma receita ou recomendação de como usar o Git para realizar o trabalho de maneira consistente e produtiva. Há vários Git Workflows, nosso projeto utilizará o git flow.

O Git Flow trabalha com dois branchs principais, a Develop e Main, que duram para sempre e quatro branchs de suporte, Feature, Release, Bugfix e Hotfix, que duram até realizar o merge com as branchs principais.

Abaixo a descrição dos principais branchs:

- Main: Aqui é onde temos todo o código de produção. Todas as novas funcionalidades que estamos desenvolvendo, em algum momento, serão mescladas ou associadas a Main.
- Develop: Aqui é onde fica o código do nosso próximo deploy. Isso significa que, como recursos ou funcionalidades, será finalizado e adicionado nesta ramificação para posteriormente passar por mais uma etapa antes de ser associado a uma Main.

Abaixo a descrição dos branchs de suporte, no dia-dia do desenvolvimento os desenvolvedores devem criar estes branchs:

- Feature-#issuexx – são branches para o desenvolvimento de funcionalidades específica. Elas devem ter o nome iniciado por feature, por exemplo, “feature-#issuexx / payment-system”. É importante saber que essas features branches são criadas sempre a partir da branch Develop. Portanto, quando finalizada, ela é removida após realizar o merge com a Branch Develop.
- Release – Serve como ponte para fazer o merge da Develop para a Main. Funciona como ambiente de homologação e é removida após realizar os testes e do merge com a Main. Caso haja alguma alteração, ela também deve ser sincronizada com a Develop.
- Bugfix – Uma branch criada a partir da Release para realizar correções encontradas no sistema no momento da validação, Quando concluída ela é excluída após realizar o merge com a Branch Release.
- Hotfix – Uma branch criada a partir da Main para realizar correções encontradas no sistema em produção. Quando concluída ela é excluída após realizar o merge com a Branch Main e Develop.

### Referências

- https://www.zup.com.br/blog/git-workflow
- https://medium.com/trainingcenter/utilizando-o-fluxo-git-flow-e63d5e0d5e04
- https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow

## Sobre mensagens de commits

A parte mais importante de uma mensagem de commit é o fato de que ela deve ser clara e significativa. No final, escrever boas mensagens de commit demonstra que você é um bom colaborador. Os benefícios de escrever boas mensagens de commit não se limitam apenas à sua equipe, mas se estendem a você mesmo e a colaboradores no futuro.

`git commit -m "<tipo>[escopo opcional]: <descrição>"`

Os commits tem os seguinte tipos e devem ser realizados com estes rótulos:

- feat: um novo recurso
- fix: uma correção de bug da aplicação
- chore: trabalho em progresso de uma funcionalidade
- build: alterações que afetam o sistema de build ou dependências externas
- static: alterações no conteúdo de arquivos estáticos (dados .json, imagens, etc)
- ci: alterações em nossos arquivos e scripts de configuração de CI
- docs: somente alterações na documentação
- perf: alteração de código que melhora o desempenho da aplicação e não altera a forma como o usuário utiliza a aplicação
- refactor: alteração de código, que não corrige um bug e nem altera a forma como o usuário utiliza a aplicação
- style: alterações que não afetam o significado do código (espaço em branco, formatação, ponto e vírgula, etc)
- test: adicionando testes ausentes ou corrigindo testes existentes
- revert: reverter para um commit anterior

### Referências

- https://www.conventionalcommits.org/pt-br/v1.0.0/
- https://www.freecodecamp.org/portuguese/news/como-escrever-boas-mensagens-de-commit-um-guia-pratico-do-git/


## Ambientes da aplicação

- Ambiente compartilhado de desenvolvimento
    
- Ambiente de homologação
    
